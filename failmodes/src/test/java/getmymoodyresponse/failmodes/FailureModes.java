package getmymoodyresponse.failmodes;

import hello.stub.HelloStub;
import hello.stub.HelloFactory;
import java.util.concurrent.TimeUnit;
import static org.junit.Assert.*;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class FailureModes {

     HelloStub hello = null;
     String clusterName = null;
     Exception requestException = null;

     public FailureModes() throws Exception {
          String helloFactoryClassName = System.getenv("HELLO_FACTORY_CLASS_NAME");
          if (helloFactoryClassName == null) throw new Exception(
               "Env variable HELLO_FACTORY_CLASS_NAME is not defined");
          HelloFactory helloFactory = (HelloFactory)
               (Class.forName(helloFactoryClassName)
               .getDeclaredConstructor().newInstance());
          this.hello = helloFactory.createHello();
     }

     private static void updateDesiredCount(String clusterName,
          String serviceName, int count) throws Exception {

	  performShellCommand(
               "aws ecs update-service --desired-count " + count + " " +
               "--cluster \"" + clusterName + "\" " +
               "--service \"" + serviceName + "\"");
     }


     private static void performShellCommand(String command) throws Exception {

          Process process = Runtime.getRuntime().exec(command);
          long ms = 5000;
          boolean finished = process.waitFor(ms, TimeUnit.MILLISECONDS);
          if (! finished) throw new Exception(
               "Command did not complete in " + ms + "ms");
          int exitValue = process.exitValue();
          if (exitValue != 0) throw new Exception(
               "Command exited with status " + exitValue);
     }

     private class Timer {
          Thread t = new Thread();
          boolean isSleeping = false;
          Timer(int milliseconds, Runnable doIfInterrupted) {
              try {
                   t.sleep(milliseconds);
                   isSleeping = true;
                   t.join();
              }
              catch (InterruptedException ex) { doIfInterrupted.run(); }
          }
          public void cancel() { t.interrupt(); isSleeping = false; }
          boolean isWaiting() { return this.isSleeping; }
     }

     @Given("the Hello and WeGood services are deployed in cluster {string} and available")
     public void hello_and_wegood_are_available(String clusterName) throws Exception {
          this.clusterName = clusterName;
          String response = null;
          for (int i = 0; i < 3; i++) {
		try { response = hello.hello(); } catch (Exception ex) {
      System.err.println("trying again");
			new Timer(1000, () -> {}); // wait one second
			continue; // try again
		}
		break;
	}
     }

     Timer timer = null;
     @Given("WeGood becomes unavailable for {int} seconds")
     public void wegood_becomes_unavailable_for(int seconds) throws Exception {
          /* Inject failure: stop the WeGood service for up to 2 seconds, or after
               the next test step completes - which ever completes first. */
          String clusterName = "hello-cluster";
          String serviceName = "wegood-service";
          updateDesiredCount(clusterName, serviceName, 0);
          this.timer = new Timer(2000, () -> { try {
               updateDesiredCount(clusterName, serviceName, 2); }
               catch (Exception ex) { ex.printStackTrace(); }});
     }

     String response = null;
     @When("I make a request to Hello")
     public void make_request_to_hello() throws Exception {
          try { response = hello.hello(); }
          catch (Exception ex) {
               this.requestException = ex;
          }
     }


     @Then("I receive a runtime exception response from {string} indicating a server problem")
     public void runtime_exception_response(String serviceName) throws Exception {
          if (this.requestException == null)
               throw new RuntimeException("Expected an exception");
          if (! (this.requestException instanceof RuntimeException))
               throw new RuntimeException("Expected a runtime exception");
          if (timer.isWaiting()) {
               this.timer.cancel();
               // Restore the instance count:
               updateDesiredCount(clusterName, serviceName, 2);
          }
     }
}
