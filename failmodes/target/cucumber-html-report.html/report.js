$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/FailureModes.feature");
formatter.feature({
  "name": "Failure Modes",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Failuremode"
    }
  ]
});
formatter.scenario({
  "name": "WeGood Temporarily Unavailable",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Failuremode"
    }
  ]
});
formatter.step({
  "name": "the Hello and WeGood services are deployed in cluster \"moody-cluster\" and available",
  "keyword": "Given "
});
formatter.match({
  "location": "FailureModes.hello_and_wegood_are_available(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "WeGood becomes unavailable for 2 seconds",
  "keyword": "And "
});
formatter.match({
  "location": "FailureModes.wegood_becomes_unavailable_for(int)"
});
formatter.result({
  "error_message": "java.lang.Exception: Command exited with status 255\r\n\tat getmymoodyresponse.failmodes.FailureModes.performShellCommand(FailureModes.java:45)\r\n\tat getmymoodyresponse.failmodes.FailureModes.updateDesiredCount(FailureModes.java:30)\r\n\tat getmymoodyresponse.failmodes.FailureModes.wegood_becomes_unavailable_for(FailureModes.java:85)\r\n\tat ✽.WeGood becomes unavailable for 2 seconds(file:src/test/resources/FailureModes.feature:5)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "I make a request to Hello",
  "keyword": "When "
});
formatter.match({
  "location": "FailureModes.make_request_to_hello()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I receive a runtime exception response from \"hello\" indicating a server problem",
  "keyword": "Then "
});
formatter.match({
  "location": "FailureModes.runtime_exception_response(String)"
});
formatter.result({
  "status": "skipped"
});
});